#!/usr/bin/env node

const express = require('express');
const http = require('http');

var options = {
  host: 'myexternalip.com',
  path: '/raw'
};

const app = express();
const port = process.env.PORT;

// logging middleware
app.use((req, res, next) => {
  console.log(`[${req.socket.remoteAddress}] ${req.method} ${req.url} : ${res.statusCode}`);
  next();
})

app.get('/', (req, res) => {
  req.accepts('application/json');
  res.status(200).send('Hello World!');
});

app.get('/ip', (req, res) => {
  req.accepts('application/json');
  http.get(options, (prox) => {
    let payload = '';
    prox.on('data', (data) => {
      payload += data;
    })
    prox.on('end', () => {
      res.status(200).send(`My IP is: ${payload}!`);
    })
  })
});

app.get('/version', (req, res) => {
  req.accepts('application/json');
  res.status(200).send({ version: 3 });
});

const server = app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
});

module.exports = { server: server }