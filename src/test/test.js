#!/usr/bin/env mocha

const chai = require('chai');
const mocha = require('mocha');
const chaiHttp = require('chai-http');
const { server } = require('../app/app');

chai.use(chaiHttp);

mocha.describe('API Test', () => {
  it('It should return Hello World!', (done) => {
    chai.request(server)
      .get('/')
      .end((err, res) => {
        chai.expect(err).to.be.equal(null);
        chai.expect(res.status).to.be.equal(200);
        chai.expect(res.text).to.be.equal('Hello World!');
        done();
      });
  })
  it('It should return the correct Version', (done) => {
    chai.request(server)
      .get('/version')
      .end((err, res) => {
        chai.expect(err).to.be.equal(null);
        chai.expect(res.status).to.be.equal(200);
        chai.expect(res.body).to.have.property('version').equal(3);
        done();
      });
  })
})
