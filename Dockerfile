# Base image
FROM node:12-alpine

# Define ports
ARG PORT=5000
ENV PORT=${PORT}
EXPOSE ${PORT}

# Define running environment
ARG NODE_ENV=production
ENV NODE_ENV=$NODE_ENV

# Set up work directory
WORKDIR /app

# Install packages
COPY package.json .
RUN npm install

# Copy project files
COPY . .

# Run the server
CMD [ "npm", "start" ]
