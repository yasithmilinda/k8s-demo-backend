# K8S Demo (Backend)
**Sample project for CS895: DevOps, Containers, and Cloud**

This project provides the following,

* a web server (using Express.JS framework)
* a test suite for that web server (using Mocha and Chai frameworks).
* a Dockerfile (to run the web server within a container)

To run the lab assignment for **Lesson Plan 5: Kubernetes and CI/CD**, please **fork** this repository and continue the assignment **on that fork**. Otherwise, you won't have access to modify this project.
