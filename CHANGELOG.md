# Changelog

## Nov 29, 2020
* Pushed the project to GitLab
* Created test cases
* Updated the Dockerfile
